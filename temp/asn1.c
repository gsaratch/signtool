
#include "asn1.h"
void encode(unsigned char *data,int inLength, unsigned char tagValue,struct TLV *tlv){
	unsigned char firstByte;
	unsigned char secondByte=0x80;
	unsigned int numberOfBytes=0;
	int i;
	tlv->tag=tagValue;
	if(inLength<0x7F){
		numberOfBytes++;
	}
	else{
		i=inLength;
		while(i!=0){
			i/=256;
			numberOfBytes++;
		}
		secondByte=0x80|numberOfBytes;
	}
	
	if(numberOfBytes==1){
		tlv->length=numberOfBytes;
		tlv->lengthBytes=(unsigned char*) malloc(sizeof(unsigned char)*numberOfBytes);
		tlv->lengthBytes[0]=(unsigned char)inLength;

	}
	else{
		tlv->length=numberOfBytes+1;
		tlv->lengthBytes=(unsigned char*) malloc(sizeof(unsigned char)*(numberOfBytes+1));
		tlv->lengthBytes[0]=secondByte;
		i=inLength;
		for(int j=0;j<numberOfBytes;j++)
		{
			tlv->lengthBytes[numberOfBytes-j]=(unsigned char)i%256;
			i/=256;
		}	
	}
	tlv->valueLength=inLength;
	tlv->value=(unsigned char*)malloc(sizeof(unsigned char)*inLength);
	for(i=0;i<inLength;i++){
		tlv->value[i]=data[i];
	}
}



void display(struct TLV *tlv){
	int i;
	printf("\n%02x ",tlv->tag);
	for(i=0;i<tlv->length;i++){
	printf("%02x ",tlv->lengthBytes[i]);
	}
	for(i=0;i<tlv->valueLength;i++){
	printf("%02x ",tlv->value[i]);
	}
	printf("\n");
	
}
void tlvToStream(unsigned char *data, int length, struct TLV *tlv){
	
	int i,count=0;
	data[count++]=tlv->tag;
	for(i=0;i<tlv->length;i++){
		data[count++]=tlv->lengthBytes[i];
	}
	for(i=0;i<tlv->valueLength;i++){
		data[count++]=tlv->value[i];
	}
}

void tlvToStream1(unsigned char **data, int *length, struct TLV *tlv){
	
	int i,count=0;
	*length=tlv->length+tlv->valueLength+1;
	if(*data)
		free(*data);

	*data=(unsigned char*)malloc(sizeof(unsigned char)*(*length));
	*(*data+count++)=tlv->tag;
	//data[count++]=tlv->tag;
	for(i=0;i<tlv->length;i++){
		//data[count++]=tlv->lengthBytes[i];
		*(*data+count++)=tlv->lengthBytes[i];
	}
	for(i=0;i<tlv->valueLength;i++){
		//data[count++]=tlv->value[i];
		*(*data+count++)=tlv->value[i];
	}
}



void version(struct TLV *tlv)
{
	struct TLV version_tlv;
	unsigned char *data=NULL;
	int dataLength;
	unsigned char version_data[1]={0x02};
	encode(version_data,1,0x02,&version_tlv);

	dataLength=version_tlv.length+version_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&version_tlv);

	encode(data,dataLength,0xA0,tlv);

}

void serialNumber(struct TLV *tlv)
{
	unsigned char certificateSerialNumber[9]={0x00, 0x9C, 0x5C, 0x68, 0x8F, 0xE5, 0x75, 0xC8, 0x57};	
	encode(certificateSerialNumber,9,0x02,tlv);
}

void signatureAlgorithm(struct TLV *tlv)
{
	struct TLV algorithmIdentifier_tlv,null_tlv;
	unsigned char oid_algorithmIdentifier[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x0B};
	unsigned char *data=NULL;
	int dataLength,i;

	encode(oid_algorithmIdentifier,9,0x06,&algorithmIdentifier_tlv);
	encode(NULL,0,0x05,&null_tlv);

	dataLength=algorithmIdentifier_tlv.length+algorithmIdentifier_tlv.valueLength+1+null_tlv.length+null_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	
	i=algorithmIdentifier_tlv.length+algorithmIdentifier_tlv.valueLength+1;

	tlvToStream(data,i,&algorithmIdentifier_tlv);
	tlvToStream(data+i,dataLength-i,&null_tlv);

	encode(data,dataLength,0x30,tlv);
	free(data);
}
void issuer()
{
	struct TLV oid_common_name_tlv,oid_country_name_tlv,oid_state_province_name_tlv,oid_organization_name_tlv,oid_organization_unit_name_tlv;
	struct TLV oid_common_name_data_tlv,oid_country_name_data_tlv,oid_state_province_name_data_tlv,oid_organization_name_data_tlv,oid_organization_unit_name_data_tlv;
	struct TLV oid_common_name_tlv_final,oid_country_name_tlv_final,oid_state_province_name_tlv_final,oid_organization_name_tlv_final,oid_organization_unit_name_tlv_final;
	struct TLV myIssuer;

	unsigned char *data=NULL,*data1=NULL,*data2=NULL,*dataFinal[5];

	int dataLength,data1Length,data2Length,i,dataLengthFinal[5];

	unsigned char oid_common_name[3]={0x55,0x04,0x03};
	unsigned char oid_country_name[3]={0x55,0x04,0x06};
	unsigned char oid_state_province_name[3]={0x55,0x04,0x08};
	unsigned char oid_organization_name[3]={0x55,0x04,0x0A};
	unsigned char oid_organization_unit_name[3]={0x55,0x04,0x0B};
	
	unsigned char oid_common_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_country_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_state_province_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_organization_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_organization_unit_name_data[3]={0x61,0x61,0x61};

	encode(oid_common_name,3,0x06,&oid_common_name_tlv);
	encode(oid_country_name,3,0x06,&oid_country_name_tlv);
	encode(oid_state_province_name,3,0x06,&oid_state_province_name_tlv);
	encode(oid_organization_name,3,0x06,&oid_organization_name_tlv);
	encode(oid_organization_unit_name,3,0x06,&oid_organization_unit_name_tlv);

	encode(oid_common_name_data,3,0x13,&oid_common_name_data_tlv);
	encode(oid_country_name_data,3,0x13,&oid_country_name_data_tlv);
	encode(oid_state_province_name_data,3,0x13,&oid_state_province_name_data_tlv);
	encode(oid_organization_name_data,3,0x13,&oid_organization_name_data_tlv);
	encode(oid_organization_unit_name_data,3,0x13,&oid_organization_unit_name_data_tlv);

	data1Length=oid_common_name_tlv.length+oid_common_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_common_name_tlv);

	data2Length=oid_common_name_data_tlv.length+oid_common_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_common_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_common_name_tlv_final);
	free(data);
	free(data1);
	free(data2);


	data1Length=oid_country_name_tlv.length+oid_country_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_country_name_tlv);

	data2Length=oid_country_name_data_tlv.length+oid_country_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_country_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_country_name_tlv_final);
	free(data);
	free(data1);
	free(data2);

	data1Length=oid_state_province_name_tlv.length+oid_state_province_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_state_province_name_tlv);

	data2Length=oid_state_province_name_data_tlv.length+oid_state_province_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_state_province_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_state_province_name_tlv_final);
	free(data);
	free(data1);
	free(data2);



	data1Length=oid_organization_name_tlv.length+oid_organization_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_organization_name_tlv);

	data2Length=oid_organization_name_data_tlv.length+oid_organization_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_organization_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_organization_name_tlv_final);
	free(data);
	free(data1);
	free(data2);




	data1Length=oid_organization_unit_name_tlv.length+oid_organization_unit_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_organization_unit_name_tlv);

	data2Length=oid_organization_unit_name_data_tlv.length+oid_organization_unit_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_organization_unit_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_organization_unit_name_tlv_final);
	free(data);
	free(data1);
	free(data2);



	dataLengthFinal[0]=oid_common_name_tlv_final.length+oid_common_name_tlv_final.valueLength+1;
	dataFinal[0]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[0]);
	tlvToStream(dataFinal[0],dataLengthFinal[0],&oid_common_name_tlv_final);

	dataLengthFinal[1]=oid_country_name_tlv_final.length+oid_country_name_tlv_final.valueLength+1;
	dataFinal[1]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[1]);
	tlvToStream(dataFinal[1],dataLengthFinal[1],&oid_country_name_tlv_final);

	dataLengthFinal[2]=oid_state_province_name_tlv_final.length+oid_state_province_name_tlv_final.valueLength+1;
	dataFinal[2]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[2]);
	tlvToStream(dataFinal[2],dataLengthFinal[2],&oid_state_province_name_tlv_final);

	dataLengthFinal[3]=oid_organization_name_tlv_final.length+oid_organization_name_tlv_final.valueLength+1;
	dataFinal[3]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[3]);
	tlvToStream(dataFinal[3],dataLengthFinal[3],&oid_organization_name_tlv_final);

	dataLengthFinal[4]=oid_organization_unit_name_tlv_final.length+oid_organization_unit_name_tlv_final.valueLength+1;
	dataFinal[4]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[4]);
	tlvToStream(dataFinal[4],dataLengthFinal[4],&oid_organization_unit_name_tlv_final);

	dataLength=0;
	for(i=0;i<5;i++){
		dataLength+=dataLengthFinal[i];
	}

	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	int count=0;
	for(int j=0;j<5;j++){
		for(i=0;i<dataLengthFinal[j];i++){
			data[count++]=dataFinal[j][i];
		}
	}

	encode(data,dataLength,0x80,&myIssuer);
	free(data);

}

void issuerSubjectInfo(struct TLV *tlv, bool flag)
{
	unsigned char issuerValues[6][30]={{"countryName"},{"stateOrProvinceName"},{"localityName"},{"organizationalUnitName"},{"organizationName"},{"commonName"}};
	unsigned char oids[6][30]={{0x55,0x04,0x06},{0x55,0x04,0x08},{0x55,0x04,0x07},{0x55,0x04,0x0B},{0x55,0x04,0x0A},{0x55,0x04,0x03}};
	unsigned char oids_data[6][20];
	
	struct TLV oids_tlv[6],oids_data_tlv[6],oids_final_tlv[6],oids_final_set_tlv[6];
	
	//unsigned char tempString[2]="aa";

	char *data=NULL,*data1=NULL,*data2=NULL;
	int dataLength,data1Length,data2Length;
	if(flag)
		printf("\n           Enter Issuer Details             ");
	else
		printf("\n           Enter Subject Details            ");	

	for(int i=0;i<6;i++){
		printf("\n%s: ",issuerValues[i]);
		scanf("%s",oids_data[i]);
		//memcpy(oids_data[i],tempString,2);
	}

	for(int i=0;i<6;i++){
		encode(oids[i],strlen(oids[i]),0x06,&oids_tlv[i]);
	}
	

	for(int i=0;i<6;i++){
		encode(oids_data[i],strlen(oids_data[i]),0x13,&oids_data_tlv[i]);
	}

	for(int i=0;i<6;i++){
		data1Length=oids_tlv[i].length+oids_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_tlv[i]);

		data2Length=oids_data_tlv[i].length+oids_data_tlv[i].valueLength+1;
		data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
		tlvToStream(data2,data2Length,&oids_data_tlv[i]);

		dataLength=data1Length+data2Length;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

		memcpy(data,data1,data1Length);
		memcpy(data+data1Length,data2,data2Length);

		encode(data,dataLength,0x30,&oids_final_tlv[i]);
		free(data1);
		free(data2);
		free(data);
	}

	for(int i=0;i<6;i++){
		dataLength=oids_final_tlv[i].length+oids_final_tlv[i].valueLength+1;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
		tlvToStream(data,dataLength,&oids_final_tlv[i]);
		encode(data,dataLength,0x31,&oids_final_set_tlv[i]);
		free(data);
	}
	dataLength=0;
	for(int i=0;i<6;i++){
		dataLength+=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
	}
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int j=0;
	for(int i=0;i<6;i++){
		data1Length=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_final_set_tlv[i]);
		
		memcpy(data+j,data1,data1Length);
		j+=data1Length;
		free(data1);
	}

	encode(data,dataLength,0x30,tlv);
}

void issuerSubjectInfoFromConfigForSS(struct TLV *tlv, bool flag,char *fileName)
{
	unsigned char issuerValues[6][30]={{"countryName"},{"stateOrProvinceName"},{"localityName"},{"organizationalUnitName"},{"organizationName"},{"commonName"}};
	unsigned char oids[6][30]={{0x55,0x04,0x06},{0x55,0x04,0x08},{0x55,0x04,0x07},{0x55,0x04,0x0B},{0x55,0x04,0x0A},{0x55,0x04,0x03}};
	unsigned char oids_data[6][20];
	
	struct TLV oids_tlv[6],oids_data_tlv[6],oids_final_tlv[6],oids_final_set_tlv[6];

	FILE *fp=NULL;
	char line[100],*key,*value;
	
	//unsigned char tempString[2]="aa";

	char *data=NULL,*data1=NULL,*data2=NULL;
	int dataLength,data1Length,data2Length;
	/*if(flag)
		printf("\n           Enter Issuer Details             ");
	else
		printf("\n           Enter Subject Details            ");	
	*/
	fp=fopen(fileName,"r");
	if(fp!=NULL){
		while(fgets(line,sizeof(line),fp)!=NULL){
			key=strtok(line," = ");
			
			value=strtok(NULL," = ");
			value=strtok(value,"\n");
			
			for(int i=0;i<6;i++){
				if(strcmp(issuerValues[i],key)==0){
					strcpy(oids_data[i],value);
				}
			}

		}
	}
	
	if(fp)
	{
		fclose(fp);
	}
	/*for(int i=0;i<6;i++){
		printf("\n%s: ",issuerValues[i]);
		scanf("%s",oids_data[i]);
		//memcpy(oids_data[i],tempString,2);
	}*/

	for(int i=0;i<6;i++){
		encode(oids[i],strlen(oids[i]),0x06,&oids_tlv[i]);
	}
	

	for(int i=0;i<6;i++){
		encode(oids_data[i],strlen(oids_data[i]),0x13,&oids_data_tlv[i]);
	}

	for(int i=0;i<6;i++){
		data1Length=oids_tlv[i].length+oids_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_tlv[i]);

		data2Length=oids_data_tlv[i].length+oids_data_tlv[i].valueLength+1;
		data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
		tlvToStream(data2,data2Length,&oids_data_tlv[i]);

		dataLength=data1Length+data2Length;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

		memcpy(data,data1,data1Length);
		memcpy(data+data1Length,data2,data2Length);

		encode(data,dataLength,0x30,&oids_final_tlv[i]);
		free(data1);
		free(data2);
		free(data);
	}

	for(int i=0;i<6;i++){
		dataLength=oids_final_tlv[i].length+oids_final_tlv[i].valueLength+1;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
		tlvToStream(data,dataLength,&oids_final_tlv[i]);
		encode(data,dataLength,0x31,&oids_final_set_tlv[i]);
		free(data);
	}
	dataLength=0;
	for(int i=0;i<6;i++){
		dataLength+=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
	}
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int j=0;
	for(int i=0;i<6;i++){
		data1Length=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_final_set_tlv[i]);
		
		memcpy(data+j,data1,data1Length);
		j+=data1Length;
		free(data1);
	}

	encode(data,dataLength,0x30,tlv);
}
void validaity(struct TLV *tlv)
{
	char buff[20];
	unsigned char utcTimeNotBefore[13],utcTimeNotAfter[13];
	
	struct TLV notBefore_tlv,notAfter_tlv;

	unsigned char *data;
	int dataLength;
	
	time_t now = time(NULL);
	strftime(buff, 20, "%y%m%d%H%M%S", localtime(&now));
	
	for(int i=0;i<12;i++){
		utcTimeNotBefore[i]=buff[i]|0x30;
		utcTimeNotAfter[i]=buff[i]|0x30;
	}
	utcTimeNotBefore[12]='Z';
	utcTimeNotAfter[12]='Z';

	utcTimeNotAfter[1]+=0x01;

	encode(utcTimeNotBefore,13,0x17,&notBefore_tlv);
	encode(utcTimeNotAfter,13,0x17,&notAfter_tlv);

	dataLength=notBefore_tlv.length+notBefore_tlv.valueLength+1+notAfter_tlv.length+notAfter_tlv.valueLength+1;
	
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int temp=notBefore_tlv.length+notBefore_tlv.valueLength+1;
	tlvToStream(data,temp,&notBefore_tlv);
	tlvToStream(data+temp,dataLength-temp,&notAfter_tlv);
	

	encode(data,dataLength,0x30,tlv);
		
}

void subjectPublicKeyInfo(struct TLV *subjectPublicKeyInfo_tlv,unsigned char modulus[],int modulus_length,unsigned char exponent[],int exponent_length)
{
	unsigned char oid_rsa_algo[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01};
	struct TLV oid_rsa_algo_tlv,null_tlv,oid_rsa_algo_final_tlv,oid_rsa_key_data_tlv,oid_rsa_key_data_final_tlv,oid_rsa_modulus_data_tlv,oid_rsa_exponent_data_tlv,oid_rsa_key_tlv;
	int oid_rsa_algo_length=9;
	//unsigned char modulus[257]={0x00},exponent[3]={0x00};
	unsigned char *data=NULL;
	int dataLength,i=0;

	encode(oid_rsa_algo,oid_rsa_algo_length,0x06,&oid_rsa_algo_tlv);
	encode(NULL,0,0x05,&null_tlv);
	
	dataLength=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1+null_tlv.length+null_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_tlv);
	tlvToStream(data+i,dataLength-i,&null_tlv);

	encode(data,dataLength,0x30,&oid_rsa_algo_final_tlv);
	free(data);

	

	encode(modulus,modulus_length,0x02,&oid_rsa_modulus_data_tlv);
	encode(exponent,exponent_length,0x02,&oid_rsa_exponent_data_tlv);

	dataLength=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1+oid_rsa_exponent_data_tlv.length+oid_rsa_exponent_data_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_modulus_data_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_exponent_data_tlv);

	encode(data,dataLength,0x30,&oid_rsa_key_data_tlv);

	free(data);

	dataLength=oid_rsa_key_data_tlv.length+oid_rsa_key_data_tlv.valueLength+1+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	data[0]=0x00;
	tlvToStream(data+1,dataLength,&oid_rsa_key_data_tlv);
	encode(data,dataLength,0x03,&oid_rsa_key_data_final_tlv);

	free(data);

	dataLength=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1+oid_rsa_key_data_final_tlv.length+oid_rsa_key_data_final_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_final_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_key_data_final_tlv);

	encode(data,dataLength,0x30,subjectPublicKeyInfo_tlv);

}

void extensions(struct TLV *tlv)
{
	struct TLV keyUsage_tlv,basicConstraints_tlv,extensions_tlv;
	unsigned char *data=NULL;
	int dataLength;

	keyUsage(&keyUsage_tlv);
	basicConstraints(&basicConstraints_tlv);

	dataLength=keyUsage_tlv.length+keyUsage_tlv.valueLength+1+basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,keyUsage_tlv.length+keyUsage_tlv.valueLength+1,&keyUsage_tlv);
	tlvToStream(data+keyUsage_tlv.length+keyUsage_tlv.valueLength+1,basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1,&basicConstraints_tlv);

	encode(data,dataLength,0x30,&extensions_tlv);

	tlvToStream1(&data,&dataLength,&extensions_tlv);

	encode(data,dataLength,0xA3,tlv);

}

void tbsder(struct TLV *tlv)
{
	struct TLV temp[10];
	
	unsigned char *data=NULL;
	int dataLength,i;
	int numberOfFields=8;

	version(&temp[0]);      //version(&version_tlv);
	serialNumber(&temp[1]);  //serialNumber(&serialNumber_tlv);
	signatureAlgorithm(&temp[2]); //signature(&signature_tlv);
	issuerSubjectInfo(&temp[3],true); //issuerSubjectInfo(&issuer_tlv);
	validaity(&temp[4]); //validaity(&validity_tlv);
	issuerSubjectInfo(&temp[5],false); //issuerSubjectInfo(&subject_tlv);
	subjectPublicKeyInfo(&temp[6],NULL,0,NULL,0); //subjectPublicKeyInfo(&subjectPublicKeyInfo_tlv);
	extensions(&temp[7]); //extensions(&extensions_tlv);

	dataLength=0;
	for(i=0;i<numberOfFields;i++){
		dataLength+=temp[i].length+temp[i].valueLength+1;
		//printf("\ndataLength=%d",dataLength);
	}
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	if(data==NULL){
		printf("\nMemory allocation failed");
	}
	int t=0;
	for(i=0;i<numberOfFields;i++){
		//printf("\nt=%d dataLength=%d",t,dataLength);

		tlvToStream(data+t,temp[i].length+temp[i].valueLength+1,&temp[i]);
		t+=temp[i].length+temp[i].valueLength+1;
	}
	encode(data,dataLength,0x30,tlv);
	free(data);

}

void keyUsage(struct TLV *tlv){
	unsigned char keyBits[3]={0x07,0x00,0x00};
	unsigned char keyUsage_oid[3]={0x55, 0x1D, 0x0F};
	struct TLV keyUsage_oid_tlv,bool_tlv,keyBits_tlv;
	unsigned char *data=NULL;
	int dataLength;	
	unsigned char true_value[1]={0xFF},false_value[1]={0x00};

	keyBits[1]^=0x84; //This is the hardcoded value for CA
	encode(keyBits,3,0x03,&keyBits_tlv);
	dataLength=keyBits_tlv.length+keyBits_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&keyBits_tlv);

	encode(data,dataLength,0x04,&keyBits_tlv);
	free(data);

	encode(keyUsage_oid,3,0x06,&keyUsage_oid_tlv);
	encode(true_value,1,0x01,&bool_tlv);

	dataLength=keyBits_tlv.length+keyBits_tlv.valueLength+1+bool_tlv.length+bool_tlv.valueLength+1+keyUsage_oid_tlv.length+keyUsage_oid_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	

	int temp=keyUsage_oid_tlv.length+keyUsage_oid_tlv.valueLength+1;
	tlvToStream(data,temp,&keyUsage_oid_tlv);
	tlvToStream(data+temp,bool_tlv.length+bool_tlv.valueLength+1,&bool_tlv);
	temp+=bool_tlv.length+bool_tlv.valueLength+1;
	tlvToStream(data+temp,keyBits_tlv.length+keyBits_tlv.valueLength+1,&keyBits_tlv);

	encode(data,dataLength,0x30,tlv);
}

void basicConstraints(struct TLV *tlv){
	struct TLV basicConstraints_oid_tlv,bool_tlv,temp;
	unsigned char basicConstraints_oid_data[3]={0x55, 0x1D, 0x13};
	unsigned char *data=NULL;
	unsigned char true_value[1]={0xFF},false_value[1]={0x00};
	int dataLength;

	encode(basicConstraints_oid_data,3,0x06,&basicConstraints_oid_tlv);
	encode(true_value,1,0x01,&bool_tlv);
	encode(true_value,1,0x01,&temp);

	tlvToStream1(&data,&dataLength,&temp);
	encode(data,dataLength,0x30,&temp);
	tlvToStream1(&data,&dataLength,&temp);
	encode(data,dataLength,0x04,&temp);

	dataLength=basicConstraints_oid_tlv.length+basicConstraints_oid_tlv.valueLength+1+bool_tlv.length+bool_tlv.valueLength+1+temp.length+temp.valueLength+1;

	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int i=basicConstraints_oid_tlv.length+basicConstraints_oid_tlv.valueLength+1;
	tlvToStream(data,i,&basicConstraints_oid_tlv);
	tlvToStream(data+i,bool_tlv.length+bool_tlv.valueLength+1,&bool_tlv);
	i+=bool_tlv.length+bool_tlv.valueLength+1;
	tlvToStream(data+i,temp.length+temp.valueLength+1,&temp);

	encode(data,dataLength,0x30,tlv);


}
#include "asn1.h"
void encode(unsigned char *data,int inLength, unsigned char tagValue,struct TLV *tlv){
	unsigned char firstByte;
	unsigned char secondByte=0x80;
	unsigned int numberOfBytes=0;
	int i;
	bool flag=true;
	tlv->tag=tagValue;
	if(inLength<0x7F){
		//numberOfBytes++;
		flag=false;
	}
	else{
		i=inLength;
		while(i!=0){
			i/=256;
			numberOfBytes++;
		}
		secondByte=0x80|numberOfBytes;
	}
	
	if(flag==false){
		tlv->length=1;
		tlv->lengthBytes=(unsigned char*) malloc(sizeof(unsigned char));
		tlv->lengthBytes[0]=(unsigned char)inLength;

	}
	else{
		tlv->length=numberOfBytes+1;
		tlv->lengthBytes=(unsigned char*) malloc(sizeof(unsigned char)*(numberOfBytes+1));
		tlv->lengthBytes[0]=secondByte;
		i=inLength;
		for(int j=0;j<numberOfBytes;j++)
		{
			tlv->lengthBytes[numberOfBytes-j]=(unsigned char)i%256;
			i/=256;
		}	
	}
	tlv->valueLength=inLength;
	tlv->value=(unsigned char*)malloc(sizeof(unsigned char)*inLength);
	for(i=0;i<inLength;i++){
		tlv->value[i]=data[i];
	}
}



void display(struct TLV *tlv){
	int i;
	if(tlv){
	printf("\n%02x ",tlv->tag);
	for(i=0;i<tlv->length;i++){
	printf("%02x ",tlv->lengthBytes[i]);
	}
	for(i=0;i<tlv->valueLength;i++){
	printf("%02x ",tlv->value[i]);
	}
	printf("\n");
	}
}
void tlvToStream(unsigned char *data, int length, struct TLV *tlv){
	
	int i,count=0;
	data[count++]=tlv->tag;
	for(i=0;i<tlv->length;i++){
		data[count++]=tlv->lengthBytes[i];
	}
	for(i=0;i<tlv->valueLength;i++){
		data[count++]=tlv->value[i];
	}
}

void tlvToStream1(unsigned char **data, int *length, struct TLV *tlv){
	
	int i,count=0;
	*length=tlv->length+tlv->valueLength+1;
	if(*data)
		free(*data);

	*data=(unsigned char*)malloc(sizeof(unsigned char)*(*length));
	*(*data+count++)=tlv->tag;
	//data[count++]=tlv->tag;
	for(i=0;i<tlv->length;i++){
		//data[count++]=tlv->lengthBytes[i];
		*(*data+count++)=tlv->lengthBytes[i];
	}
	for(i=0;i<tlv->valueLength;i++){
		//data[count++]=tlv->value[i];
		*(*data+count++)=tlv->value[i];
	}
}



void version(struct TLV *tlv)
{
	struct TLV version_tlv;
	unsigned char *data=NULL;
	int dataLength;
	unsigned char version_data[1]={0x02};
	encode(version_data,1,0x02,&version_tlv);

	dataLength=version_tlv.length+version_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&version_tlv);

	encode(data,dataLength,0xA0,tlv);

}

void serialNumber(struct TLV *tlv)
{
	unsigned char certificateSerialNumber[9]={0x00, 0x9C, 0x5C, 0x68, 0x8F, 0xE5, 0x75, 0xC8, 0x57};	
	
	unsigned char temp[20],serialNumber[20]={0x00};
	int i,value=0;
	FILE *fp=NULL;
	fp=fopen("serialnumbers","r");
	if(fp){
		while(fgets(serialNumber,20,fp)!=NULL){
			memcpy(temp,serialNumber,strlen(serialNumber));
		}
		temp[strlen(temp)-1]='\0';
		value=atoi(temp);
		
		fclose(fp);

		

	}
	value+=1;
	
	sprintf(temp,"%d",value);
	fp=fopen("serialnumbers","a");
	if(fp){
		fwrite("\n",1,1,fp);
		fwrite(temp,strlen(temp),1,fp);
	fclose(fp);
	}
	encode(temp,strlen(temp),0x02,tlv);

}

void signatureAlgorithm(struct TLV *tlv)
{
	struct TLV algorithmIdentifier_tlv,null_tlv;
	unsigned char oid_algorithmIdentifier[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x0B};
	unsigned char *data=NULL;
	int dataLength,i;

	encode(oid_algorithmIdentifier,9,0x06,&algorithmIdentifier_tlv);
	encode(NULL,0,0x05,&null_tlv);

	dataLength=algorithmIdentifier_tlv.length+algorithmIdentifier_tlv.valueLength+1+null_tlv.length+null_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	
	i=algorithmIdentifier_tlv.length+algorithmIdentifier_tlv.valueLength+1;

	tlvToStream(data,i,&algorithmIdentifier_tlv);
	tlvToStream(data+i,dataLength-i,&null_tlv);

	encode(data,dataLength,0x30,tlv);
	free(data);
}
void issuer()
{
	struct TLV oid_common_name_tlv,oid_country_name_tlv,oid_state_province_name_tlv,oid_organization_name_tlv,oid_organization_unit_name_tlv;
	struct TLV oid_common_name_data_tlv,oid_country_name_data_tlv,oid_state_province_name_data_tlv,oid_organization_name_data_tlv,oid_organization_unit_name_data_tlv;
	struct TLV oid_common_name_tlv_final,oid_country_name_tlv_final,oid_state_province_name_tlv_final,oid_organization_name_tlv_final,oid_organization_unit_name_tlv_final;
	struct TLV myIssuer;

	unsigned char *data=NULL,*data1=NULL,*data2=NULL,*dataFinal[5];

	int dataLength,data1Length,data2Length,i,dataLengthFinal[5];

	unsigned char oid_common_name[3]={0x55,0x04,0x03};
	unsigned char oid_country_name[3]={0x55,0x04,0x06};
	unsigned char oid_state_province_name[3]={0x55,0x04,0x08};
	unsigned char oid_organization_name[3]={0x55,0x04,0x0A};
	unsigned char oid_organization_unit_name[3]={0x55,0x04,0x0B};
	
	unsigned char oid_common_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_country_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_state_province_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_organization_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_organization_unit_name_data[3]={0x61,0x61,0x61};

	encode(oid_common_name,3,0x06,&oid_common_name_tlv);
	encode(oid_country_name,3,0x06,&oid_country_name_tlv);
	encode(oid_state_province_name,3,0x06,&oid_state_province_name_tlv);
	encode(oid_organization_name,3,0x06,&oid_organization_name_tlv);
	encode(oid_organization_unit_name,3,0x06,&oid_organization_unit_name_tlv);

	encode(oid_common_name_data,3,0x13,&oid_common_name_data_tlv);
	encode(oid_country_name_data,3,0x13,&oid_country_name_data_tlv);
	encode(oid_state_province_name_data,3,0x13,&oid_state_province_name_data_tlv);
	encode(oid_organization_name_data,3,0x13,&oid_organization_name_data_tlv);
	encode(oid_organization_unit_name_data,3,0x13,&oid_organization_unit_name_data_tlv);

	data1Length=oid_common_name_tlv.length+oid_common_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_common_name_tlv);

	data2Length=oid_common_name_data_tlv.length+oid_common_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_common_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_common_name_tlv_final);
	free(data);
	free(data1);
	free(data2);


	data1Length=oid_country_name_tlv.length+oid_country_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_country_name_tlv);

	data2Length=oid_country_name_data_tlv.length+oid_country_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_country_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_country_name_tlv_final);
	free(data);
	free(data1);
	free(data2);

	data1Length=oid_state_province_name_tlv.length+oid_state_province_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_state_province_name_tlv);

	data2Length=oid_state_province_name_data_tlv.length+oid_state_province_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_state_province_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_state_province_name_tlv_final);
	free(data);
	free(data1);
	free(data2);



	data1Length=oid_organization_name_tlv.length+oid_organization_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_organization_name_tlv);

	data2Length=oid_organization_name_data_tlv.length+oid_organization_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_organization_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_organization_name_tlv_final);
	free(data);
	free(data1);
	free(data2);




	data1Length=oid_organization_unit_name_tlv.length+oid_organization_unit_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_organization_unit_name_tlv);

	data2Length=oid_organization_unit_name_data_tlv.length+oid_organization_unit_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_organization_unit_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_organization_unit_name_tlv_final);
	free(data);
	free(data1);
	free(data2);



	dataLengthFinal[0]=oid_common_name_tlv_final.length+oid_common_name_tlv_final.valueLength+1;
	dataFinal[0]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[0]);
	tlvToStream(dataFinal[0],dataLengthFinal[0],&oid_common_name_tlv_final);

	dataLengthFinal[1]=oid_country_name_tlv_final.length+oid_country_name_tlv_final.valueLength+1;
	dataFinal[1]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[1]);
	tlvToStream(dataFinal[1],dataLengthFinal[1],&oid_country_name_tlv_final);

	dataLengthFinal[2]=oid_state_province_name_tlv_final.length+oid_state_province_name_tlv_final.valueLength+1;
	dataFinal[2]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[2]);
	tlvToStream(dataFinal[2],dataLengthFinal[2],&oid_state_province_name_tlv_final);

	dataLengthFinal[3]=oid_organization_name_tlv_final.length+oid_organization_name_tlv_final.valueLength+1;
	dataFinal[3]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[3]);
	tlvToStream(dataFinal[3],dataLengthFinal[3],&oid_organization_name_tlv_final);

	dataLengthFinal[4]=oid_organization_unit_name_tlv_final.length+oid_organization_unit_name_tlv_final.valueLength+1;
	dataFinal[4]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[4]);
	tlvToStream(dataFinal[4],dataLengthFinal[4],&oid_organization_unit_name_tlv_final);

	dataLength=0;
	for(i=0;i<5;i++){
		dataLength+=dataLengthFinal[i];
	}

	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	int count=0;
	for(int j=0;j<5;j++){
		for(i=0;i<dataLengthFinal[j];i++){
			data[count++]=dataFinal[j][i];
		}
	}

	encode(data,dataLength,0x80,&myIssuer);
	free(data);

}

void issuerSubjectInfo(struct TLV *tlv, bool flag)
{
	int numberOfFields=9;
	unsigned char issuerValues[numberOfFields][30]={{"countryName"},{"stateOrProvinceName"},{"localityName"},{"organizationalUnitName"},{"organizationName"},{"commonName"},{"title"},{"description"}};
	unsigned char oids[numberOfFields][30]={{0x55,0x04,0x06},{0x55,0x04,0x08},{0x55,0x04,0x07},{0x55,0x04,0x0B},{0x55,0x04,0x0A},{0x55,0x04,0x03},{0x55,0x04,0x0C},{0x55,0x04,0x0D},{0x2A,0x86,0x48,0x86,0xF7,0x0D,0x01,0x09,0x01}};
	unsigned char oids_data[numberOfFields][100];
	
	struct TLV oids_tlv[numberOfFields],oids_data_tlv[numberOfFields],oids_final_tlv[numberOfFields],oids_final_set_tlv[numberOfFields];
	
	//unsigned char tempString[2]="aa";

	char *data=NULL,*data1=NULL,*data2=NULL;
	int dataLength,data1Length,data2Length;
	if(flag)
		printf("\n           Enter Issuer Details             ");
	else
		printf("\n           Enter Subject Details            ");	
	getchar();
	for(int i=0;i<numberOfFields;i++){
		printf("\n%s: ",issuerValues[i]);
		fgets(oids_data[i],30,stdin);
		oids_data[i][strcspn(oids_data[i],"\n")]=0;
	}

	for(int i=0;i<numberOfFields;i++){
		encode(oids[i],strlen(oids[i]),0x06,&oids_tlv[i]);
	}
	
	encode(oids_data[0],strlen(oids_data[0]),0x13,&oids_data_tlv[0]);
	for(int i=1;i<numberOfFields;i++){
		encode(oids_data[i],strlen(oids_data[i]),0x0C,&oids_data_tlv[i]);
	}

	

	for(int i=0;i<numberOfFields;i++){
		data1Length=oids_tlv[i].length+oids_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_tlv[i]);

		data2Length=oids_data_tlv[i].length+oids_data_tlv[i].valueLength+1;
		data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
		tlvToStream(data2,data2Length,&oids_data_tlv[i]);

		dataLength=data1Length+data2Length;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

		memcpy(data,data1,data1Length);
		memcpy(data+data1Length,data2,data2Length);

		encode(data,dataLength,0x30,&oids_final_tlv[i]);
		free(data1);
		free(data2);
		free(data);
	}

	for(int i=0;i<numberOfFields;i++){
		dataLength=oids_final_tlv[i].length+oids_final_tlv[i].valueLength+1;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
		tlvToStream(data,dataLength,&oids_final_tlv[i]);
		encode(data,dataLength,0x31,&oids_final_set_tlv[i]);
		free(data);
	}
	dataLength=0;
	for(int i=0;i<numberOfFields;i++){
		dataLength+=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
	}
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int j=0;
	for(int i=0;i<numberOfFields;i++){
		data1Length=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_final_set_tlv[i]);
		
		memcpy(data+j,data1,data1Length);
		j+=data1Length;
		free(data1);
	}

	encode(data,dataLength,0x30,tlv);
}

void issuerSubjectInfoFromConfigForSS(struct TLV *tlv, bool flag,char *fileName)
{
	int numberOfFields=9;
	//unsigned char issuerValues[numberOfFields][30]={{"IcountryName"},{"IstateOrProvinceName"},{"IlocalityName"},{"IorganizationalUnitName"},{"IorganizationName"},{"IcommonName"},{"Ititle"},{"Idescription"},{"IemailAddress"}};
	//unsigned char subjectValues[numberOfFields][30]={{"ScountryName"},{"SstateOrProvinceName"},{"SlocalityName"},{"SorganizationalUnitName"},{"SorganizationName"},{"ScommonName"},{"Stitle"},{"Sdescription"},{"SemailAddress"}};
	unsigned char issuerSubjectValues[numberOfFields][30]={{"countryName"},{"stateOrProvinceName"},{"localityName"},{"organizationalUnitName"},{"organizationName"},{"commonName"},{"title"},{"description"},{"emailAddress"}};
	unsigned char oids[numberOfFields][30]={{0x55,0x04,0x06},{0x55,0x04,0x08},{0x55,0x04,0x07},{0x55,0x04,0x0B},{0x55,0x04,0x0A},{0x55,0x04,0x03},{0x55,0x04,0x0C},{0x55,0x04,0x0D},{0x2A,0x86,0x48,0x86,0xF7,0x0D,0x01,0x09,0x01}};
	unsigned char oids_data[numberOfFields][100]={0x00};
	bool include_oids[9]={true,false,true,true,true,true,false,false,false};
	struct TLV oids_tlv[numberOfFields],oids_data_tlv[numberOfFields],oids_final_tlv[numberOfFields],oids_final_set_tlv[numberOfFields];

	FILE *fp=NULL;
	char line[100],*key,*value;
	
	
	char *data=NULL,*data1=NULL,*data2=NULL;
	int dataLength,data1Length,data2Length;
	/*if(flag)
		printf("\n           Enter Issuer Details             ");
	else
		printf("\n           Enter Subject Details            ");	
	*/

	fp=fopen(fileName,"r");
	if(fp==NULL){
		printf("\nOpening of file %s is failed",fileName);
	}
	if(fp!=NULL){

		while(fgets(line,sizeof(line),fp)!=NULL){
			key=strtok(line,"=");
			if((key!=NULL)&&(strlen(key)!=0)){
				key[strlen(key)-1]='\0';
			}
			
			value=strtok(NULL,"\n");
			
			
			//if(flag) {
			for(int i=0;i<numberOfFields;i++){
				if(strcmp(issuerSubjectValues[i],key)==0){
					strcpy(oids_data[i],value+1);
				}
			}
		/*}
		else {
			for(int i=0;i<numberOfFields;i++){
				if(strcmp(subjectValues[i],key)==0){
					strcpy(oids_data[i],value);
				}
			}	
		}*/

	}
 	}
	
	if(fp)
	{
		fclose(fp);
	}
	
	/*printf("\nDetails are:\n");

	for(int i=0;i<numberOfFields;i++){
		printf("\n%s:%s\n",issuerSubjectValues[i],oids_data[i]);
	}*/
	
	for(int i=0;i<numberOfFields;i++){
		encode(oids[i],strlen(oids[i]),0x06,&oids_tlv[i]);
	}
	
	encode(oids_data[0],strlen(oids_data[0]),0x13,&oids_data_tlv[0]);
	for(int i=1;i<numberOfFields;i++){
		encode(oids_data[i],strlen(oids_data[i]),0x0C,&oids_data_tlv[i]);
	}

	for(int i=0;i<numberOfFields;i++){

		data1Length=oids_tlv[i].length+oids_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_tlv[i]);

		data2Length=oids_data_tlv[i].length+oids_data_tlv[i].valueLength+1;
		data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
		tlvToStream(data2,data2Length,&oids_data_tlv[i]);

		dataLength=data1Length+data2Length;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

		memcpy(data,data1,data1Length);
		memcpy(data+data1Length,data2,data2Length);

		encode(data,dataLength,0x30,&oids_final_tlv[i]);
		free(data1);
		free(data2);
		free(data);
	}

	for(int i=0;i<numberOfFields;i++){
		dataLength=oids_final_tlv[i].length+oids_final_tlv[i].valueLength+1;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
		tlvToStream(data,dataLength,&oids_final_tlv[i]);
		encode(data,dataLength,0x31,&oids_final_set_tlv[i]);
		free(data);
	}
	dataLength=0;
	for(int i=0;i<numberOfFields;i++){
		if(include_oids[i])
		dataLength+=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
	}
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int j=0;
	for(int i=0;i<numberOfFields;i++){
		if(include_oids[i]){
		data1Length=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_final_set_tlv[i]);
		
		memcpy(data+j,data1,data1Length);
		j+=data1Length;
		free(data1);
	}
	}

	encode(data,dataLength,0x30,tlv);
}

void validity(struct TLV *tlv)
{
	char buff[20];
	//unsigned char utcTimeNotBefore[13],utcTimeNotAfter[13];
	unsigned char generalizedTimeBefore[15],generalizedTimeAfter[15];
	
	struct TLV notBefore_tlv,notAfter_tlv;

	unsigned char *data;
	int dataLength;
	
	time_t now = time(NULL);
	strftime(buff, 20, "%Y%m%d%H%M%S", localtime(&now));
	buff[5]=buff[5]-1; //adjusting the time to 1 day prior to avoid clock synchronization while veryfying
	//printf("\n%s",buff);
 /*
	
	for(int i=0;i<12;i++){
		utcTimeNotBefore[i]=buff[i]|0x30;
		utcTimeNotAfter[i]=buff[i]|0x30;
	}
	utcTimeNotBefore[12]='Z';
	utcTimeNotAfter[12]='Z';

	utcTimeNotAfter[1]+=0x01;
 */
	for(int i=0;i<14;i++){
		generalizedTimeBefore[i]=buff[i]|0x30;
		generalizedTimeAfter[i]=buff[i]|0x30;
	}
		generalizedTimeBefore[14]='Z';
		generalizedTimeAfter[14]='Z';

		generalizedTimeAfter[0]=0x03|0x30;
		generalizedTimeAfter[1]=0x00|0x30;
		generalizedTimeAfter[2]=0x01|0x30;
		generalizedTimeAfter[3]=0x05|0x30;

	//encode(utcTimeNotBefore,13,0x17,&notBefore_tlv);
	//encode(utcTimeNotAfter,13,0x17,&notAfter_tlv);
	
	encode(generalizedTimeBefore,15,0x18,&notBefore_tlv);
	encode(generalizedTimeAfter,15,0x18,&notAfter_tlv);


	dataLength=notBefore_tlv.length+notBefore_tlv.valueLength+1+notAfter_tlv.length+notAfter_tlv.valueLength+1;
	
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int temp=notBefore_tlv.length+notBefore_tlv.valueLength+1;
	tlvToStream(data,temp,&notBefore_tlv);
	tlvToStream(data+temp,dataLength-temp,&notAfter_tlv);
	

	encode(data,dataLength,0x30,tlv);
		
}

void subjectPublicKeyInfo(struct TLV *subjectPublicKeyInfo_tlv,unsigned char modulus[],int modulus_length,unsigned char exponent[],int exponent_length)
{
	unsigned char oid_rsa_algo[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01};
	struct TLV oid_rsa_algo_tlv,null_tlv,oid_rsa_algo_final_tlv,oid_rsa_key_data_tlv,oid_rsa_key_data_final_tlv,oid_rsa_modulus_data_tlv,oid_rsa_exponent_data_tlv,oid_rsa_key_tlv;
	int oid_rsa_algo_length=9;
	unsigned char *data=NULL;
	int dataLength,i=0;

	encode(oid_rsa_algo,oid_rsa_algo_length,0x06,&oid_rsa_algo_tlv);
	encode(NULL,0,0x05,&null_tlv);
	
	dataLength=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1+null_tlv.length+null_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_tlv);
	tlvToStream(data+i,dataLength-i,&null_tlv);

	encode(data,dataLength,0x30,&oid_rsa_algo_final_tlv);
	free(data);

	

	encode(modulus,modulus_length,0x02,&oid_rsa_modulus_data_tlv);
	encode(exponent,exponent_length,0x02,&oid_rsa_exponent_data_tlv);

	dataLength=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1+oid_rsa_exponent_data_tlv.length+oid_rsa_exponent_data_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_modulus_data_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_exponent_data_tlv);

	encode(data,dataLength,0x30,&oid_rsa_key_data_tlv);

	free(data);

	dataLength=oid_rsa_key_data_tlv.length+oid_rsa_key_data_tlv.valueLength+1+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	data[0]=0x00;
	tlvToStream(data+1,dataLength,&oid_rsa_key_data_tlv);
	encode(data,dataLength,0x03,&oid_rsa_key_data_final_tlv);

	free(data);

	dataLength=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1+oid_rsa_key_data_final_tlv.length+oid_rsa_key_data_final_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_final_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_key_data_final_tlv);

	encode(data,dataLength,0x30,subjectPublicKeyInfo_tlv);

}

void subjectPublicKeyInfoFromFile(struct TLV *subjectPublicKeyInfo_tlv,char *fileName,unsigned char exponent[],int exponent_length)
{
	unsigned char oid_rsa_algo[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01};
	struct TLV oid_rsa_algo_tlv,null_tlv,oid_rsa_algo_final_tlv,oid_rsa_key_data_tlv,oid_rsa_key_data_final_tlv,oid_rsa_modulus_data_tlv,oid_rsa_exponent_data_tlv,oid_rsa_key_tlv;
	int oid_rsa_algo_length=9;
	unsigned char *data=NULL;
	int dataLength,i=0;

	encode(oid_rsa_algo,oid_rsa_algo_length,0x06,&oid_rsa_algo_tlv);
	encode(NULL,0,0x05,&null_tlv);
	
	dataLength=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1+null_tlv.length+null_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_tlv);
	tlvToStream(data+i,dataLength-i,&null_tlv);

	encode(data,dataLength,0x30,&oid_rsa_algo_final_tlv);
	free(data);

	
	readModulus(&oid_rsa_modulus_data_tlv,fileName);
	//encode(modulus,modulus_length,0x02,&oid_rsa_modulus_data_tlv);
	encode(exponent,exponent_length,0x02,&oid_rsa_exponent_data_tlv);

	dataLength=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1+oid_rsa_exponent_data_tlv.length+oid_rsa_exponent_data_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_modulus_data_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_exponent_data_tlv);

	encode(data,dataLength,0x30,&oid_rsa_key_data_tlv);

	free(data);

	dataLength=oid_rsa_key_data_tlv.length+oid_rsa_key_data_tlv.valueLength+1+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	data[0]=0x00;
	tlvToStream(data+1,dataLength,&oid_rsa_key_data_tlv);
	encode(data,dataLength,0x03,&oid_rsa_key_data_final_tlv);

	free(data);

	dataLength=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1+oid_rsa_key_data_final_tlv.length+oid_rsa_key_data_final_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_final_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_key_data_final_tlv);

	encode(data,dataLength,0x30,subjectPublicKeyInfo_tlv);

}


void extensions(struct TLV *tlv,bool flag)
{
	struct TLV keyUsage_tlv,basicConstraints_tlv,extensions_tlv;
	unsigned char *data=NULL;
	int dataLength;

	keyUsage(&keyUsage_tlv,flag);
	basicConstraints(&basicConstraints_tlv,flag);

	dataLength=keyUsage_tlv.length+keyUsage_tlv.valueLength+1+basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,keyUsage_tlv.length+keyUsage_tlv.valueLength+1,&keyUsage_tlv);
	tlvToStream(data+keyUsage_tlv.length+keyUsage_tlv.valueLength+1,basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1,&basicConstraints_tlv);

	encode(data,dataLength,0x30,&extensions_tlv);

	tlvToStream1(&data,&dataLength,&extensions_tlv);

	encode(data,dataLength,0xA3,tlv);

}

void tbsder(struct TLV *tlv,bool flag)
{
	struct TLV temp[10];
	
	unsigned char *data=NULL;
	int dataLength,i;
	int numberOfFields=8;

	version(&temp[0]);      //version(&version_tlv);
	serialNumber(&temp[1]);  //serialNumber(&serialNumber_tlv);
	signatureAlgorithm(&temp[2]); //signature(&signature_tlv);
	issuerSubjectInfo(&temp[3],true); //issuerSubjectInfo(&issuer_tlv);
	validity(&temp[4]); //validaity(&validity_tlv);
	issuerSubjectInfo(&temp[5],false); //issuerSubjectInfo(&subject_tlv);
	subjectPublicKeyInfo(&temp[6],NULL,0,NULL,0); //subjectPublicKeyInfo(&subjectPublicKeyInfo_tlv);
	extensions(&temp[7],flag); //extensions(&extensions_tlv);

	dataLength=0;
	for(i=0;i<numberOfFields;i++){
		dataLength+=temp[i].length+temp[i].valueLength+1;
		//printf("\ndataLength=%d",dataLength);
	}
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	if(data==NULL){
		printf("\nMemory allocation failed");
	}
	int t=0;
	for(i=0;i<numberOfFields;i++){
		//printf("\nt=%d dataLength=%d",t,dataLength);

		tlvToStream(data+t,temp[i].length+temp[i].valueLength+1,&temp[i]);
		t+=temp[i].length+temp[i].valueLength+1;
	}
	encode(data,dataLength,0x30,tlv);
	free(data);

}

void keyUsage(struct TLV *tlv,bool flag){
	unsigned char keyBits[3]={0x07,0x00,0x00};
	unsigned char keyUsage_oid[3]={0x55, 0x1D, 0x0F};
	struct TLV keyUsage_oid_tlv,bool_tlv,keyBits_tlv;
	unsigned char *data=NULL;
	int dataLength;	
	unsigned char true_value[1]={0xFF},false_value[1]={0x00};
	if(flag)
		keyBits[1]^=0xC4; //This is the hardcoded value for CA for digital signature, non repudiation, keycert sign
	else
		keyBits[1]^=0xC0;

	encode(keyBits,3,0x03,&keyBits_tlv);
	dataLength=keyBits_tlv.length+keyBits_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&keyBits_tlv);

	encode(data,dataLength,0x04,&keyBits_tlv);
	free(data);

	encode(keyUsage_oid,3,0x06,&keyUsage_oid_tlv);
	if(flag)
		encode(true_value,1,0x01,&bool_tlv);
	else
		encode(false_value,1,0x01,&bool_tlv);

	dataLength=keyBits_tlv.length+keyBits_tlv.valueLength+1+bool_tlv.length+bool_tlv.valueLength+1+keyUsage_oid_tlv.length+keyUsage_oid_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	

	int temp=keyUsage_oid_tlv.length+keyUsage_oid_tlv.valueLength+1;
	tlvToStream(data,temp,&keyUsage_oid_tlv);
	tlvToStream(data+temp,bool_tlv.length+bool_tlv.valueLength+1,&bool_tlv);
	temp+=bool_tlv.length+bool_tlv.valueLength+1;
	tlvToStream(data+temp,keyBits_tlv.length+keyBits_tlv.valueLength+1,&keyBits_tlv);

	encode(data,dataLength,0x30,tlv);
}

void basicConstraints(struct TLV *tlv,bool flag){
	struct TLV basicConstraints_oid_tlv,bool_tlv,temp;
	unsigned char basicConstraints_oid_data[3]={0x55, 0x1D, 0x13};
	unsigned char *data=NULL;
	unsigned char true_value[1]={0xFF},false_value[1]={0x00};
	int dataLength;

	encode(basicConstraints_oid_data,3,0x06,&basicConstraints_oid_tlv);
	if(flag)
		encode(true_value,1,0x01,&bool_tlv);
	else
		encode(false_value,1,0x01,&bool_tlv);
	if(flag)
		encode(true_value,1,0x01,&temp);
	else
		encode(false_value,1,0x01,&temp);

	tlvToStream1(&data,&dataLength,&temp);
	encode(data,dataLength,0x30,&temp);
	tlvToStream1(&data,&dataLength,&temp);
	encode(data,dataLength,0x04,&temp);

	dataLength=basicConstraints_oid_tlv.length+basicConstraints_oid_tlv.valueLength+1+bool_tlv.length+bool_tlv.valueLength+1+temp.length+temp.valueLength+1;

	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int i=basicConstraints_oid_tlv.length+basicConstraints_oid_tlv.valueLength+1;
	tlvToStream(data,i,&basicConstraints_oid_tlv);
	tlvToStream(data+i,bool_tlv.length+bool_tlv.valueLength+1,&bool_tlv);
	i+=bool_tlv.length+bool_tlv.valueLength+1;
	tlvToStream(data+i,temp.length+temp.valueLength+1,&temp);

	encode(data,dataLength,0x30,tlv);


}

int getVal(char ch)
{
	if ((ch >= '0') && (ch <= '9')){
		return ch - '0';
	}
	else if((ch >= 'a') && (ch <= 'f')){
		return ch - 'a' +10;
	}
	else if((ch >= 'A') && (ch <= 'F')){
		return ch - 'A' +10;
	}	
	 	
	
}
void readModulus(struct TLV *tlv, char *fileName)
{
	FILE *fp=NULL;
	fp=fopen(fileName,"r");
	unsigned char key[257];
	int i=1;
	char ch,ch1;
	key[0]=0x00;
	if(fp){
		while((ch=fgetc(fp))!=EOF){
			ch1=fgetc(fp);
			if(ch1==EOF)
				break;
			key[i++]=getVal(ch)*16+getVal(ch1);
			}
		fclose(fp);
		encode(key,257,0x02,tlv);

	}
}
/**************************************************************************************************
 *
 * Filename           : keystore.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Key Store that stores external keys (Key Blobs)
 *
 *************************************************************************************************/
#ifndef __KEYSTORE_H
#define __KEYSTORE_H

#include "cxi.h"

namespace cxi
{      
  class CXIAPI KeyStore
  {
    private:      
      int idx_len;      
      void *p_db;   
      
      void init(void);
      void open(char *filename, int idx_len);
      
    public:
      KeyStore(void);
      KeyStore(char *filename, int idx_len);
      KeyStore(Config &config, int idx_len);       
      virtual ~KeyStore(void);

      int getIndexLength();
      
      /**
       * Modes for KeyStore::findKey
       */
      enum modes
      {
        MODE_EQUAL = 0, //!< search key with exactly the given index
        MODE_GTEQ,      //!< search key with the given or next greater index
        MODE_GREATER,   //!< search key with the next greater index
      };

      bool findKey(ByteArray &startIndex, int mode, PropertyList &keyTemplate = NULL_REF(PropertyList));
      
      Key getKey(ByteArray &index);

      void insertKey(int flags, ByteArray &index, Key &key);
      
      void deleteKey(ByteArray &index);
  };
}

#endif

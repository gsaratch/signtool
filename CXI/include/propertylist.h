/**************************************************************************************************
 *
 * Filename           : propertylist.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Encapsulates Array of Property Objects
 *
 *************************************************************************************************/
#ifndef __PROPERTYLIST_H
#define __PROPERTYLIST_H

#include "cxi.h"

namespace cxi
{     
  class CXIAPI PropertyList
  {  
    private:
      Property proptab[128];
      
      void checkDate(char *date);
      time_t dateToTime(char *date, int len);
      void timeToDate(time_t timer, char *date);

    public:
      int getMaxProperties(void) { return sizeof(proptab)/sizeof(proptab[0]); }

      PropertyList(void);
      PropertyList(unsigned char *p_data, unsigned int l_data);
      PropertyList(const ByteArray &data);
      PropertyList(const PropertyList &pl);
      virtual ~PropertyList(void);
      
      void clear(void);
      void parse(unsigned char *p_data, unsigned int l_data);
      void parse(const ByteArray &pl);
      void merge(const PropertyList &pl);      
      ByteArray serialize(void);
      
      static ByteArray find(int idx, const ByteArray &pl);

      // get property values      
      const Property &get(int idx);
            
      int    getAlgo(void);      
      int    getSize(void);
      char   *getCurve(void);
      char   *getGroup(void);
      char   *getName(void);
      int    getExport(void);
      int    getUsage(void);      
      int    getSpecifier(void);      
      char   *getLabel(void);
      int    getBlockLength(void);      
      int    getType(void);
      char   *getDate(void);
      char   *getDateGen(void);
      char   *getDateExp(void);      
      time_t getTime(void);
      time_t getTimeGen(void);
      time_t getTimeExp(void);
      
      ByteArray getUName(void);
      
      // set property values
      void  set(int idx, const Property &property);

      void  setAlgo(int algo);        
      void  setSize(int size);           
      void  setCurve(char *curve);
      void  setGroup(const char *group); 
      void  setName(const char *name);         
      void  setExport(int expo);         
      void  setUsage(int usage);
      void  setSpecifier(int spec);      
      void  setLabel(char *label);       
      void  setType(int type);
      void  setDate(char *date);
      void  setDateGen(char *date);      
      void  setDateExp(char *date);            
      void  setTime(time_t timer);
      void  setTimeGen(time_t timer);
      void  setTimeExp(time_t timer);
      
      // overloaded operators
      const PropertyList &operator=(const PropertyList &pl);
      const PropertyList &operator|=(const PropertyList &pl);      
  };   
}

#endif


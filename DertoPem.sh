#!/bin/sh
derDirectory="pem"
if [ ! -e $derDirectory ]
	then
	mkdir $derDirectory
fi

for i in *.der
do
		var=$(echo $i | cut -d '.' -f 1)".pem"
		var="./pem/$var"
		openssl x509 -inform der -outform pem -in $i -out $var
done
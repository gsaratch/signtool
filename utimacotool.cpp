#include<iostream>
#include<cxi.h>
#include<stdio.h>
#include<string.h>
#include<openssl/evp.h>

using namespace std;
using namespace cxi;


/******************************************************************************
 *
 * Version
 *
 ******************************************************************************/
#define CXI_DEMO_VERSION  "1.0.1"
#define CXI_DEMO_DATE     __DATE__

/******************************************************************************
 *
 * Macros
 *
 ******************************************************************************/
#define CLEANUP(e) { err = (e); goto cleanup; }
#define DIM(x)     (sizeof((x))/sizeof((x[0])))

#ifndef MIN
#define MIN(a,b)   ((a)<(b)?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b)   ((a)>(b)?(a):(b))
#endif

#define BUF_SIZE 100

/******************************************************************************
 *
 * Globals 
 *
 ******************************************************************************/
static char *AlgoNames[] = { (char*)"RAW", (char*)"DES", (char*)"AES", (char*)"RSA", (char*)"ECDSA", (char*)"DSA", (char*)"ECDH", (char*)"DH" };

static char *Group = NULL;

/******************************************************************************
 *
 * cxi_demo_list_keys
 *
 ******************************************************************************/
void cxi_demo_list_keys(Cxi *cxi)
{    
  printf("listing all keys in group: %s ...\n", Group);
  
  PropertyList keyTemplate;
  keyTemplate.setGroup(Group);
  
  KeyList keyList = cxi->key_list(keyTemplate);

  keyList.sort();  
      
  printf("\n");
  printf("%-3s %-5s %-4s %-4s %-24s %-24s %s\n", "idx", "algo", "size", "type", "group", "name", "spec");
  printf("--------------------------------------------------------------------------------\n");
  
  for (int i=0; i<keyList.size(); i++)
  {      
    printf("%-3d %-5s %-4d %-4x %-24s %-24s %-d\n", i,                
                                                    AlgoNames[keyList[i].getAlgo() % DIM(AlgoNames)],
                                                    keyList[i].getSize(),
                                                    keyList[i].getType(),
                                                    keyList[i].getGroup(),
                                                    keyList[i].getName(),
                                                    keyList[i].getSpecifier());    
  }
  printf("OK\n");
}

/*============================================================================
** Function Name:    readFromFile

** Description:      This Function reads the data byte by byte from the input file and
					 stores it in the buffer.
** Inputs:          fileName - The name of the file from which data needs to be read.
					  retBuf - Buffer to store the file data
				  retBufSize - Size of the buffer

** Outputs:          1 - On successful reading of the data from the file
					 0 - On Failure	
**==========================================================================*/
int readFromFile(char *fileName, char **retBuf,int *retBufSize)
{

	FILE *fp=NULL;
	int fileSize,len;
	char buf[BUF_SIZE];
	int ret=0;

	if(NULL == (fp=fopen(fileName,"rb"))) goto err;
	
	fseek(fp,0L,SEEK_END);
	fileSize=ftell(fp);
	*retBufSize=fileSize;
	fseek(fp,0L,0L);

	*retBuf=NULL;
	*retBuf=(char*)malloc(fileSize);
	if(NULL == *retBuf) goto err;

	len=0;
	while(fread(buf,1,1,fp)) {
		strncpy(*retBuf+len,buf,1);
		len+=1;
	}
	ret=1;
err:
	if(1 != ret) {
		throw;
		/* Do Something Here*/
	}
	if(fp) fclose(fp);
	
	return ret;
}




/*============================================================================
** Function Name:    computeDigest

** Description:      This Function computes the digest of a given file.
** Inputs:          fileName - The name of the file to which digest needs to be compiuted.
					     dig - The digest that is computed
				      digLen - Size of the digest

** Outputs:          1 - On successful computation of the digest to the file
					 0 - On Failure	
**==========================================================================*/
int computeDigest(char *inFile,unsigned char *dgst,unsigned int *dgstLen)
{

//printf("\nFile name is %s",inFile);


FILE *fp=NULL,*fpWrite=NULL;
	EVP_MD_CTX ctx;
	char buf[BUF_SIZE];
	char *outFile="some_file.txt.dgst"; 
	int bufLen,ret=0,bytesRead;
	Hash hash;

	if((fp=fopen(inFile,"rb"))==NULL) goto err;
	
	
	EVP_MD_CTX_init(&ctx);

	if(1 != EVP_DigestInit_ex(&ctx, EVP_sha256(), NULL)) goto err;
	
	while(0 != (bytesRead=fread(buf,1,BUF_SIZE,fp))) {
		
		if(1 != EVP_DigestUpdate(&ctx, buf,bytesRead)) goto err;
		
	}
	
	if(1 != EVP_DigestFinal_ex(&ctx, dgst,dgstLen)) goto err;
	
	if(NULL==(fpWrite=fopen(outFile,"wb"))) goto err;

	fwrite(dgst,1,*dgstLen,fpWrite);
	
		
	ret=1;
err:
	if(ret!=1) {
		throw;
		/*Do Something Here*/
	}
	if(fp) fclose(fp);
	if(fpWrite) fclose(fpWrite);
	
	return ret;

}




/*============================================================================
** Function Name:    computeDigestFile

** Description:      This Function computes the digest of a given file.
** Inputs:          fileName - The name of the file to which digest needs to be compiuted.
					     dig - The digest that is computed
				      digLen - Size of the digest

** Outputs:          1 - On successful computation of the digest to the file
					 0 - On Failure	
**==========================================================================*/
int computeDigestFile(char *inFile,char *outFile)
{
	FILE *fp=NULL,*fpWrite=NULL;
	EVP_MD_CTX ctx;
	char buf[BUF_SIZE];
	char tempFile[100];
	unsigned char *dgst=NULL; 
	int bufLen,bytesRead,ret=0;
	unsigned int dgstLen=0;

	if((fp=fopen(inFile,"rb"))==NULL) goto err;
	
	EVP_MD_CTX_init(&ctx);

	if(1 != EVP_DigestInit_ex(&ctx, EVP_sha256(), NULL)) goto err;
	
	while(0!=(bytesRead=fread(buf,1,BUF_SIZE,fp))) {
	//	bufLen=strlen(buf);
		
		if(1 != EVP_DigestUpdate(&ctx, buf,bytesRead)) goto err;
		
	}
	dgstLen=32;
	dgst=(unsigned char *)malloc(dgstLen);
	if(NULL == dgst) goto err;
	
	if(1 != EVP_DigestFinal_ex(&ctx, dgst,&dgstLen)) goto err;
	
	if(NULL == outFile) {
		strcpy(tempFile,inFile);
		strcat(tempFile,".dgst");
		
	}
	else
		{
			strcpy(tempFile,outFile);
		}

	if(NULL==(fpWrite=fopen(tempFile,"wb"))) goto err;

	fwrite(dgst,1,dgstLen,fpWrite);
	
	ret=1;
err:
	if(ret!=1) {
		/*Do Something Here*/
		throw;
	}
	if(fp) fclose(fp);
	if(fpWrite) fclose(fpWrite);
	
	return ret;
}

int computeDigestASN1(char *inFile, char *outFile)
{
	FILE *fp=NULL;
	unsigned char dgst[BUF_SIZE]; /*The Digest size varies depending on the algorithm*/
	unsigned int dgstLen;
	int ret=0,tagLen;
	char tempFile[100];
	unsigned char tag[]={0x30, 0x31, 0x30, 0x0d, 0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 
	0x01, 0x05, 0x00, 0x04, 0x20};
	tagLen=sizeof(tag);
	/*Computes the digest of the file*/
	if(0==computeDigest(inFile,dgst,&dgstLen)) goto err;

	if(NULL==outFile) {
		strcpy(tempFile,inFile);
		strcat(tempFile,".dgst.asn1");
	}
	else {
		strcpy(tempFile,outFile);
	}
	//printf("\nDigest Length is %d\n",digLen);
	if(NULL==(fp=fopen(tempFile,"wb"))) goto err;

	

	fwrite(tag,1,tagLen,fp);

	fwrite(dgst,1,dgstLen,fp);

	ret=1;
	err:
		if(1!=ret) {
			/* Do Something Here */
			throw;
		}
	if(fp) fclose(fp);
	return ret;	
}

int signHashUtimaco(char *inFile,char *outFile)
{
	Cxi *cxi=NULL;
	PropertyList keyTemplate,key1; 
	KeyList keylist;
	Key rsakey;
	FILE *fp=NULL,*fp1=NULL;

	char *dgst=NULL;
	unsigned char *sig=NULL;
	int dgstLen=0;
	char tempFile[100];
	int i,sigLen=0,ret=0;
	bool result=false;
	ByteArray data=ByteArray(32);
	ByteArray sign;

	
	readFromFile(inFile,&dgst,&dgstLen);
	//printf("\nDigest Length: %d",dgstLen);

	cxi = new Cxi("136.18.56.57", 20000, 50000);
	cxi->logon_pass("UberAdmin", "1234", true);
	keyTemplate.setGroup(NULL);
	keylist=cxi->key_list(keyTemplate);
	key1=keylist[0];	
	rsakey = cxi->key_open(0, key1);
	MechanismParameter mechParam;

	
	
	data=ByteArray((char*)dgst,dgstLen);	
	//data.xtrace("data");
	
	// sign data
	mechParam = MechanismParameter(CXI_MECH_PAD_PKCS1|CXI_MECH_HASH_ALGO_SHA256);
	
	sign = cxi->sign(CXI_FLAG_HASH_PART,rsakey, mechParam, data);
	
	//sign.xtrace("sign");
	sigLen=sign.length();
	sig=(unsigned char *)sign.get();
	
	if(NULL==outFile)
	{
		strcpy(tempFile,inFile);
		strcat(tempFile,".dgst.sig");
	}
	else
	{
		strcpy(tempFile,outFile);
	}
	if(NULL==(fp=fopen(tempFile,"wb"))) goto err;
	
	fwrite(sig,1,sigLen,fp);
	if(fp) fclose(fp);
	

	ret=1;
	
	
	err:
		if(1 != ret)
		{
			throw;
		}	
		
	return ret;
}


int signFileUtimaco(char *inFile,char *outFile)
{
	Cxi *cxi=NULL;
	PropertyList keyTemplate,key1; 
	KeyList keylist;
	Key rsakey;
	FILE *fp=NULL,*fp1=NULL;
	MechanismParameter mechParam;
	ByteArray sign;

	unsigned char dgst[BUF_SIZE],*sig;
	unsigned int dgstLen=0;
	char tempFile[BUF_SIZE],buf[BUF_SIZE];
	int i,sigLen=0,ret=0,bytesRead;
	bool result=false;
	ByteArray data=ByteArray(32);
	Hash hash;	
		
	if(NULL==(fp1=fopen(inFile,"rb"))) goto err;
	
	// create SHA-256 hash
	
	hash.init(CXI_MECH_HASH_ALGO_SHA256);
	while(0 != (bytesRead=fread(buf,1,BUF_SIZE,fp1))) {
	//printf("\nLength of the data read:%d:%s",strlen(buf),buf);	
	hash.update(buf,bytesRead);
	}
	
	hash.final();
	// dump hash value
	
	hash.xtrace("hash");
	
	cxi = new Cxi("136.18.56.57", 20000, 50000);
	cxi->logon_pass("UberAdmin", "1234", true);
	keyTemplate.setGroup(NULL);
	keylist=cxi->key_list(keyTemplate);
	key1=keylist[0];	
	rsakey = cxi->key_open(0, key1);
	
	
	// sign data
	mechParam = MechanismParameter(CXI_MECH_PAD_PKCS1|CXI_MECH_HASH_ALGO_SHA256);
	sign = cxi->sign(rsakey, mechParam, hash);
	sign.xtrace("signature");
	sigLen=sign.length();
	sig=(unsigned char *)sign.get();
	if(NULL==outFile)
	{
		strcpy(tempFile,inFile);
		strcat(tempFile,".sig");
	}
	else
	{
		strcpy(tempFile,outFile);
	}
	if(NULL==(fp=fopen(tempFile,"wb"))) goto err;
	
	fwrite(sig,1,sigLen,fp);
	if(fp) fclose(fp);
	
	ret=1;

	
	err:
		if(1 != ret)
		{
			throw;
		}	
		
	return ret;
}




int verifyFileUtimaco(char *inFile, char *sigFile)
{
	Cxi *cxi=NULL;
	PropertyList keyTemplate,key1; 
	KeyList keylist;
	Key rsakey;
	MechanismParameter mechParam;
	FILE *fp=NULL,*fp1=NULL;
	ByteArray test_data=ByteArray("Test Message\n",13);

	unsigned char dgst[BUF_SIZE];
	unsigned int dgstLen=0;
	char tempFile[BUF_SIZE],buf[BUF_SIZE],*sig=NULL;
	int i,sigLen=0,ret=0,bytesRead;
	bool result=false;
	ByteArray data=ByteArray(32);
	ByteArray sign;//=ByteArray(512);
	Hash hash;	


	computeDigest(inFile,dgst,&dgstLen);

	if(NULL==(fp1=fopen(inFile,"rb"))) goto err;
	
	// create SHA-256 hash
	
	hash.init(CXI_MECH_HASH_ALGO_SHA256);
	while(fgets(buf,BUF_SIZE,fp1)) {
	hash.update(buf,strlen(buf));
	}
	
	hash.final();
	//hash.xtrace("hash");
	


	cxi = new Cxi("136.18.56.57", 20000, 50000);
	cxi->logon_pass("UberAdmin", "1234", true);
	keyTemplate.setGroup(NULL);
	keylist=cxi->key_list(keyTemplate);
	key1=keylist[0];	
	rsakey = cxi->key_open(0, key1);
	mechParam = MechanismParameter(CXI_MECH_PAD_PKCS1|CXI_MECH_HASH_ALGO_SHA256);

	// read signature from file
	readFromFile(sigFile,&sig,&sigLen);
	sign=ByteArray(sig,sigLen);
	//sign.xtrace("sign");
	
	data=ByteArray((char*)dgst,dgstLen);
	
	ret=cxi->verify(CXI_FLAG_HASH_PART,rsakey, mechParam, data, sign);
	err:
		if(1 != ret)
		{
			throw;	
		}
	if(fp) fclose(fp);
	if(fp1) fclose(fp1);
		
	return ret;
}

int verifyHashUtimaco(char *hashFile, char *sigFile)
{
	Cxi *cxi=NULL;
	PropertyList keyTemplate,key1; 
	KeyList keylist;
	Key rsakey;
	MechanismParameter mechParam;
	FILE *fp=NULL,*fp1=NULL;
	ByteArray test_data=ByteArray("Test Message\n",13);

	char *dgst=NULL,*sig=NULL;
	int i,dgstLen=0,sigLen=0,ret=0;
	bool result=false;
	
	ByteArray data;
	ByteArray sign;
		
	readFromFile(hashFile,&dgst,&dgstLen);
	data=ByteArray((char*)dgst,dgstLen);
	
	cxi = new Cxi("136.18.56.57", 20000, 50000);
	cxi->logon_pass("UberAdmin", "1234", true);
	
	keyTemplate.setGroup(NULL);
	keylist=cxi->key_list(keyTemplate);
	key1=keylist[0];	
	rsakey = cxi->key_open(0, key1);
	
	mechParam = MechanismParameter(CXI_MECH_PAD_PKCS1|CXI_MECH_HASH_ALGO_SHA256);

	// read signature from file
	readFromFile(sigFile,&sig,&sigLen);
	sign=ByteArray(sig,sigLen);
	//sign.xtrace("sign");
	
	
	
	ret=cxi->verify(CXI_FLAG_HASH_PART,rsakey, mechParam, data, sign);
	err:
		if(1 != ret)
		{
			throw;	
		}
	if(fp) fclose(fp);
	if(fp1) fclose(fp1);
		
	return ret;
}



int verifyFile(char *inFile, char *sigFile)
{
	Cxi *cxi=NULL;
	PropertyList keyTemplate,key1; 
	KeyList keylist;
	Key rsakey;
	FILE *fp=NULL;

	unsigned char dgst[100];
	unsigned int dgstLen=0;
	char tempFile[100],*sig=NULL;
	int i,sigLen=0,ret=0;
	bool result=false;
	ByteArray data=ByteArray(32);
	ByteArray test_data=ByteArray("Test Message\n",13);
	ByteArray sign;//=ByteArray(512);

	
	computeDigest(inFile,dgst,&dgstLen);
	printf("\nDigest Length:%d",dgstLen);

	cxi = new Cxi("136.18.56.57", 20000, 50000);
	cxi->logon_pass("UberAdmin", "1234", true);
	keyTemplate.setGroup(NULL);
	keylist=cxi->key_list(keyTemplate);
	key1=keylist[0];	
	rsakey = cxi->key_open(0, key1);
	MechanismParameter mechParam = MechanismParameter();
	mechParam = MechanismParameter(CXI_MECH_PAD_PKCS1|CXI_MECH_HASH_ALGO_SHA256);

	data=ByteArray((char*)dgst,dgstLen);
	data.xtrace("data");

	// read signature from file
	readFromFile(sigFile,&sig,&sigLen);
	sign=ByteArray(sig,sigLen);
	sign.xtrace("sign");
	
	// verify signature
	ret=cxi->verify(CXI_FLAG_HASH_DATA,rsakey, mechParam, test_data, sign);
	
	err:
		if(1 != ret)
		{
			printf("\nDo something here");	
		}	
	return ret;
}

int loginToServer(char *username,char *password,char *serverIP)
{
	Cxi *cxi=NULL;
	try {
		cxi = new Cxi(serverIP, 20000, 50000);
		cxi->logon_pass(username, password, true);
		return 1;
	}
	catch(cxi::Exception e)
	{
		return 0;
	}
}


int main(int argc, char **argv)
{
	char *inFile=NULL,*outFile=NULL,*privFile=NULL,*pubFile=NULL,*sigFile=NULL,*username=NULL,*password=NULL,*serverIP=NULL;
int operation=-1,digest=-1,asn1=0,inASN1Digest=0;
int temp=1,ret=0,opErr=0;
if(argc<2) goto err;

while(temp<argc) {
	
	if(0==strcmp(argv[temp],"-in"))	{
	
	if(temp<argc) {
		
		inFile=argv[++temp];
		
		}
	else
		goto err;
	}
	
	else if(0==strcmp(argv[temp],"-out")) {
	if(temp<argc)	
		outFile=argv[++temp];
	else
		goto err;
	
	}
	
	else if(0==strcmp(argv[temp],"-sig")) {
	if(temp<argc)	
		sigFile=argv[++temp];
	else
		goto err;
	
	
	}
	else if(0==strcmp(argv[temp],"-hash")) {
	if(temp<argc) {
		if(0==strcmp(argv[++temp],"sha256")) {
		digest=1;
		}
		else
			goto err;
	}	
	}
	else if(0==strcmp(argv[temp],"-uname")) {
		if(temp<argc){
			username=argv[++temp];
		}
		else
			goto err;
	}

	else if(0==strcmp(argv[temp],"-pwd")) {
		if(temp<argc){
			password=argv[++temp];
		}
		else
			goto err;
	}

	else if(0==strcmp(argv[temp],"-serverIP")) {
		if(temp<argc){
			serverIP=argv[++temp];
		}
		else
			goto err;
	}


	else if((0==strcmp(argv[temp],"sign")) && (-1 == operation)) {
		
	operation=1;
	}
	else if((0==strcmp(argv[temp],"verify")) && (-1 == operation)) {
	operation=2;
	}
	else if((0==strcmp(argv[temp],"signhash")) && (-1 == operation)) {
	operation=3;
	}
	else if((0==strcmp(argv[temp],"verifyhash")) && (-1 == operation)) {
	operation=4;
	}
	else if((0==strcmp(argv[temp],"digest")) && (-1 == operation)){
	operation=5;
	}
	else if((0==strcmp(argv[temp],"login")) && (-1 == operation)){
	operation=6;
	}
	else {
	printf("\nSomething Wrong: %s",argv[temp]);
	goto err;
	}
	temp++;
	
	}
	
	if(6==operation)
	{
		//printf("\nCredentials: %s:%s:%s",username,password,serverIP);
		printf("%d",loginToServer(username,password,serverIP));
	}
	else if(inFile) {
		if(1 == operation) {
			if(0 == signFileUtimaco(inFile,outFile)) {
						opErr=1;
						printf("\nError occured while signing");
						goto err;
						}
			
			
			
			
	 	}
		else if((2 == operation) &&(sigFile)) {
				if(1 == verifyFileUtimaco(inFile,sigFile))	{
						//printf("\nVerification is Successful");
						printf("1");
					}
				else { 
					//printf("Verificaction Failed");
					printf("0");
				}
			
		}
		else if(3 == operation) {
			if(0 == signHashUtimaco(inFile,outFile)) {
						opErr=1;
						goto err;
						}
			
					
	 	}
		
		else if((4==operation) && (sigFile)) {
			 	if(1==verifyHashUtimaco(inFile,sigFile)) {
					//printf("\n Verification is Successful");
					printf("1");
				}
				else {
					printf("\nVerification Failed");
					printf("0");
				}			
		}
		else if(5 == operation) {
			if(1 == digest) {
					if(0 == computeDigestFile(inFile,outFile)) {
						opErr=1;
						goto err;
					}
				}
			else
				goto err;
		}
		else {
			printf("\nInvalid Command");
			goto err;
		}
    }

ret=1;

err:
	if(0 == opErr) {
	if(ret==0) {
		printf("\nInvalid number of arguments\n");
		printf("\n		Usage: utimacotool [options]");
		printf("\n		Commands supported 	login, sign, verify, signhash, verifyhash, digest");
		printf("\n		-uname username -pwd password -serverIP Server IP");
		printf("\n		-in file           	Input file");
		printf("\n		-out file          	Output file");
		printf("\n		-sig file          	Signature file");
		printf("\n		-hash algo(sha256)         	Compute the Digest on -in file\n\n");
	}
	else {
		//printf("\nOperation Success");
	}
 }
	else {
		printf("\nOperation Failed");
		}
return 0;}
